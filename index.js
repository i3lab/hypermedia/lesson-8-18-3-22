
// Retrieve the title TAG
const title = document.getElementById("title")
// Get the content inside
const content = title.innerHTML
// Print it
console.log(content)

// Update the content of the h1 element
title.innerHTML = "The cat shelter Edited"

// Function that returns a string containing the template that we are going
// to append to the main id
function createCatTemplate(name, breed, img) {
    return `<div class="card">
        <div class="image-container">
            <img class="cat-img" src="${img}" />
        </div>
        <span class="cat-name">${name}</span>
        <span class="cat-breed">${breed}</span>
    </div>`
}

// Our list of cats :)
const catList = [
    {
        name: "Cat 1",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
    {
        name: "Cat 2",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
    {
        name: "Cat 3",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
]

// Retrieve the TAG main of our page
const app = document.getElementById('app')

for (let cat of catList) {
    app.innerHTML += createCatTemplate(cat.name, cat.breed, cat.img)
}

// Function called by the button in the index.html page
function parseInput() {
    const boxName = document.getElementById("name")
    const boxBreed = document.getElementById("breed")
    const boxImage = document.getElementById("img")
    const nameValue = boxName.value
    const breedValue = boxBreed.value
    const imgValue = boxImage.value
    app.innerHTML += createCatTemplate(nameValue, breedValue, imgValue)
}

